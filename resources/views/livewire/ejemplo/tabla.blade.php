<div class="p-2">

    <button wire:click="abrir_crear(1)" class="p-2 bg-blue-400 rounded-lg hover:bg-blue-500 w-fit">Registrar</button>

    <div>
        <table class="w-full text-center m-4">
            <thead class="">
                <th class="bg-slate-400">Id</th>
                <th class="bg-slate-400">Nombre</th>
                <th class="bg-slate-400">Precio</th>
                <th class="bg-slate-400">Ingredientes</th>
                <th class="bg-slate-400">Acciones</th>
            </thead>
            <tbody>
                @forelse ($comidas as $comida)
                    <tr>
                        <td>{{ $comida->id_comida }}</td>
                        <td>{{ $comida->nombre }}</td>
                        <td>{{ $comida->precio }}</td>
                        <td> <button wire:click="eliminar({{ $comida->id_comida }})"
                                class=" bg-blue-600 hover:bg-blue-700 p-2 text-white">Ver ingredientes</button>
                        </td>
                        <td>
                            <button wire:click="eliminar({{ $comida->id_comida }})"
                                class=" bg-red-600 hover:bg-red-700 p-2 text-white">Eliminar</button>
                            <button wire:click="abrir_crear(2)"
                                class=" bg-green-600 hover:bg-green-700 p-2 text-white">Editar</button>
                        </td>
                    </tr>
                @empty

                    <td class="text-center col-span-3">
                        <p class="text-red-500 text-lg">No hay comidas</p>
                    </td>
                @endforelse




            </tbody>
        </table>
    </div>

    <p>{{ $nombre_comida }}</p>
    @include('livewire.ejemplo.modal-crear')

</div>
