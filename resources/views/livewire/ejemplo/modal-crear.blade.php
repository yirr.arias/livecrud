<div class="fixed inset-0 overflow-y-auto" style="display: {{ $modalNuevaComida ? 'block' : 'none' }}" x-show="open">
    <div class="flex items-center justify-center min-h-screen">
        <div class="bg-white rounded-lg shadow-lg p-8">
            <div class=" p-2 bg-slate-200  rounded-lg shadow-lg flex flex-col gap-4 w-fit mx-auto">

                <p class="text-2xl">Crear comida</p>

                <label for="nombre_comida">Nombre</label>

                <input wire:model="nombre_comida" type="text" name="nombre_comida" id="nombre_comida" placeholder="Ingresa el nombre" class="rounded-lg w-52">

                <label for="precio_comida">Precio</label>

                <input wire:model="precio_comida" type="text" name="precio_comida" id="precio_comida" placeholder="Ingresa el nombre" class="rounded-lg w-52">


            </div>
            <div class="m-auto p-2 gap-1 flex flex-row">
                <button  wire:click='cerrar_crear()' class="bg-red-400 m-2 p-1 rounded-sm
                ">Cerrar</button>

                @if ($opcion==1)
                <button wire:click='create()'class="bg-blue-400 m-2 p-1 rounded-sm
                ">Guardar</button>
                @else
                <button wire:click='edit    ()'class="bg-blue-400 m-2 p-1 rounded-sm
                ">Editar</button>
                @endif

            </div>

        </div>
    </div>
</div>
