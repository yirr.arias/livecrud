<?php

namespace App\Http\Livewire\Ejemplo;

use App\Models\Comida;
use Livewire\Component;


class Tabla extends Component
{
    public $comidas;
    public $nombre_comida;
    public $precio_comida;

    public $opcion=1;



    //variables para abrir modales
    public  $modalNuevaComida=false;

    public function mount(){

    }
    public function render()
    {

        $this->comidas=Comida::all();
        return view('livewire.ejemplo.tabla');
    }

    public function create(){
        $this->validate([
            'nombre_comida'=> 'required',
            'precio_comida'=> 'integer|required',
        ]
        );
        comida::create([
            'nombre'=> $this->nombre_comida,
            'precio'=> $this->precio_comida
        ]);

        $this->modalNuevaComida=false;
        $this->reset();



    }

    public function abrir_crear($num)  {
        $this->opcion=$num;
        $this->modalNuevaComida=true;
    }

    public function cerrar_crear()  {
        $this->modalNuevaComida=false;
        $this->reset();

    }

    public function eliminar($id){
        Comida::find($id)->delete();
    }
}
