<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingerediente extends Model
{
    use HasFactory;

    protected $table = 'ingredientes';
    protected $primaryKey = 'id_ingrediente';
    protected $fillable = [
        'nombre',
        'id_comida',
    ];

    public $timestamps = true;
}
